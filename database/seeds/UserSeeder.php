<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
       $user = new User();
       $user->email = "admin@example.com";
       $user->password = Hash::make('123456');
       $user->created_at = Carbon\Carbon::now();
       $user->updated_at = Carbon\Carbon::now();
       $user->save();
    }
}
