<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sistema extends Model
{
    //
    protected $connection = 'system';

    protected $table='COLUMNS';


    protected $fillable = [

        'id',
        'COLUMN_NAME',
        'COLUMN_COMMENT',
        'IS_NULLABLE',
        'DATA_TYPE',
        'CHARACTER_MAXIMUM_LENGTH',
        'NUMERIC_PRECISION',
        'NUMERIC_SCALE',
        'COLUMN_TYPE',
        'COLUMN_KEY',
        'TABLE_SCHEMA',
        'TABLE_NAME',
        'ORDINAL_POSITION'

    ];
}
