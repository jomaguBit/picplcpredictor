<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePredtn extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        \DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::dropIfExists('picplc_ctg');
        Schema::dropIfExists('schedule_ctg');
        Schema::dropIfExists('restriction_ctg');

        \DB::statement('SET FOREIGN_KEY_CHECKS=1');


        Schema::create('schedule_ctg', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('range');
            $table->timestamps();
        });

        Schema::create('restriction_ctg', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('digit');
            $table->timestamps();
        });


        Schema::create('picplc_ctg', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('day');
            $table->string('permission');
            $table->integer('schedule_id')->unsigned();
            $table->integer('restriction_id')->unsigned();

            $table->foreign('schedule_id')
                  ->references('id')->on('schedule_ctg')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');

            $table->foreign('restriction_id')
                  ->references('id')->on('restriction_ctg')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');      

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('picplc_ctg');
        Schema::dropIfExists('schedule_ctg');
        Schema::dropIfExists('restriction_ctg');
    }

}
