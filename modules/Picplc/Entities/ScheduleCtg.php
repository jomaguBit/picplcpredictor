<?php namespace Modules\Picplc\Entities;
   
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScheduleCtg extends Model {

    //picplc_ctg
	protected $table='schedule_ctg';
	protected $primaryKey = 'id';

	// Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
	protected $hidden = ['created_at','updated_at'];


	public function schedule_ctg() {
		// $this hace referencia al objeto que tengamos en ese momento.
		return $this->hasMany('App\schedule_ctg');
	}
}