<?php

Route::group(['middleware' => 'web', 'prefix' => 'picplc', 'namespace' => 'Modules\Picplc\Http\Controllers'], function() {
		
	Route::resource('/picctg', 'PicplcController');


	Route::put('/picctg/{id}',
	        [
	            'uses' => 'PicplcController@update'
	        ]);

	Route::get('/picctg/{id}',
	        [
	            'uses' => 'PicplcController@show'
	        ]);

	Route::post('/picctg/validate',
	        [
	            'uses' => 'PicplcController@store'
	        ]);
});