<?php namespace Modules\Picplc\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Modules\Http\Requests;
use Modules\Picplc\Entities\PicPlcCtg;
use Modules\Picplc\Entities\RestrictionCtg;
use Modules\Picplc\Entities\ScheduleCtg;
use Illuminate\Http\Request;
use App\Http\Controllers\Nova\NovaController;

use Carbon\Carbon;

class PicplcController extends NovaController {

	function __construct(){
        parent::__construct();
	}
	

    public function index(){
        // Devolverá todos las PicPlc Registros.        
        $this->novaMessage->addInfoMessage('Show all','Mostrando todos los registros');
        $this->novaMessage->setData(['list'=>picplcctg::all()]);
        return $this->returnJSONMessage();
    }



    public function store(Request $request) {
		$digit = $request->input('digit');
		$day = $request->input('day');
		$time = $request->input('time');
		$picLst = null;
		$schLst = null;
		$resLst = null;
		$permission = '';
		
		try{
			$picLst = picplcctg::all();
			$max = sizeof($picLst);
			for($i = 0; $i < $max; $i++){
				if($picLst[$i]->day == $day){
					$schLst = schedulectg::all();
					$maxSch = sizeof($schLst);
					$rangeStr = '';
					for($j = 0; $j < $maxSch; $j++){
						$rangeStr = $schLst[0]->range;
						if($this->isBetweenRange($rangeStr,$time)){
							echo 'hubo match!';
							$resLst = restrictionctg::find($picLst[$i]->restriction_id);
							if($this->isBetweenDigit($resLst->digit, $digit))
								$permission = 'n';
							else
								$permission = 'y';
						} else {
							$permission = 'y';
						}
					}

				} else {
					$permission = 'y';
				}
			}

		} catch(\Exception $e){
        	return response(json_encode(['error' => $e->getMessage()]), $e->getCode())
                ->header("Access-Control-Allow-Origin"," *");
		}
		$this->novaMessage->setData(['permission'=>$permission]);			
		return $this->returnJSONMessage(200);		
	}


	public function isBetweenRange($sourceRg, $queValue){	
		$rangeStr = $sourceRg;
		$rangeArr = explode (".", $rangeStr);
		$valueFrt = explode ("h", $rangeArr[0]);
		$valueSnd = explode ("h", $rangeArr[1]);
		$valueQue = explode ("h", $queValue);

		$hourFrt = (int) $valueFrt[0];
		$secondFrt = (int) $valueFrt[1];
		$timeFtFrt = (float) $hourFrt + $secondFrt/60;

		$hourSnd = (int) $valueSnd[0];
		$secondSnd = (int) $valueSnd[1];
		$timeFtSnd = (float) $hourSnd + $secondSnd/60;

		$hourQue = (int) $valueQue[0];
		$secondQue = (int) $valueQue[1];
		$timeFtQue = (float) $hourQue + $secondQue/60;
		echo "" . $timeFtQue;

		if((bccomp($timeFtQue, $timeFtFrt, 3) == 1  || 
			bccomp($timeFtQue, $timeFtFrt, 3) == 0) && 
		   (bccomp($timeFtSnd, $timeFtQue, 3) == 1  || 
			bccomp($timeFtSnd, $timeFtQue, 3) == 0)
		  )
			return true;
		else
			return false;

		return false;
	}


	public function isBetweenDigit($sourceDg, $queValue){	
		$digitArr = explode (",", $sourceDg);
		$maxDigit = sizeof($digitArr);
		for($p = 0; $p < $maxDigit; $p++){
			if(strcmp ($digitArr[$p] , $queValue) == 0)
				return true;
		}
		return false;
	}	
	
}