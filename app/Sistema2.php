<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sistema2 extends Model
{
    //
    protected $connection = 'system';

    protected $table='KEY_COLUMN_USAGE';


    protected $fillable = [
        'CONSTRAINT_SCHEMA',
        'CONSTRAINT_NAME',
        'TABLE_NAME',
        'COLUMN_NAME',
        'REFERENCED_TABLE_NAME',
        'REFERENCED_COLUMN_NAME',

    ];

}
