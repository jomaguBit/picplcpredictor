<?php namespace App;

use Zizaco\Entrust\EntrustRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Modules\Configuration\Entities\Department;
use Modules\Configuration\Entities\Bank;
use Modules\Configuration\Entities\FilePersonalCategory;
use Modules\Configuration\Entities\Holidays;
use Modules\Configuration\Entities\Kinship;
use Modules\Configuration\Entities\Period;
use Modules\Configuration\Entities\Profession;
use Modules\Configuration\Entities\TypeAmount;
use Modules\Configuration\Entities\TypeContract;
use Modules\Configuration\Entities\TypeOperation;
use Modules\Configuration\Entities\TypePayment;
use Modules\Configuration\Entities\TypeSalary;
use Modules\Configuration\Entities\TypeVacation;


trait ValidateDescriptionModel{

	public function validateName($name, $model)
	{
		
  	    foreach ($model as $key => $value) {
  	    	
     	  if ($name ==$value['name'])
          {
            return 1;
          }
          
    	}
   
	}



}
