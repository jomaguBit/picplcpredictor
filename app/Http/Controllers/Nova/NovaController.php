<?php

namespace App\Http\Controllers\Nova;

use App\NovaMessage;
use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use App\Http\Middleware\AddJsonAcceptHeader;

class NovaController extends Controller
{
	public $novaMessage;

	function __construct()
	{
		$this->novaMessage=new NovaMessage();
		//$this->middleware('addJSONHeader');
	}

	/*header('Access-Control-Allow-Origin: http://sub.example.com');
	header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
	header('Access-Control-Allow-Headers: Accept, X-Requested-With');
	// without credentials we can use * for origin
	header('Access-Control-Allow-Credentials: true');
	header('HTTP/1.1 200 OK', true);*/

	function returnJSONMessage($responseCode=200){
		return response($this->novaMessage->toJSON(),$responseCode)
                    		->header('Content-Type', 'application/json')
                    		->header("Access-Control-Allow-Origin"," *")
							->header('Access-Control-Allow-Methods','GET, POST, PUT, OPTIONS, DELETE')
							->header('Access-Control-Request-Headers','Accept, X-Requested-With')
							->header('HTTP/1.1 200 OK', true)
							->header('Access-Control-Allow-Credentials',true);
	}
}