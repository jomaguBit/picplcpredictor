<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/', function () {
    return view('welcome');
});
*/
Route::post('auth/authenticate',
    	'Nova\AuthenticateController@authenticate');

Route::post('auth/logout', 
    	'Nova\AuthenticateController@logout');

Route::post('auth/refresh-token', ['middleware' => 'jwt.refresh', function() {}]);

Route::get('auth/user', 
		'Nova\AuthenticateController@getAuthenticatedUser');